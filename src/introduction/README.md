# Veloren: An Owner's Manual

This manual is the primary source of documentation for Veloren, both for users and for developers. It aims to document the features of Veloren, the manner in which it is developed, and describe the inner workings of both the game and the engine. It also aims to discuss future plans the development team has for Veloren.

![Image of Veloren](https://media.discordapp.net/attachments/634860358623821835/1135611839389106236/screenshot_1690819371981.png)
| _Taken by @bidgehop_

![Image of Veloren](https://media.discordapp.net/attachments/634860358623821835/1135611839779180596/screenshot_1690819743770.png)
| _Taken by @bidgehop_

![Image of Veloren](https://media.discordapp.net/attachments/634860358623821835/1135614124143943741/screenshot_1690821862890.png)
| _Taken by @bidgehop_
